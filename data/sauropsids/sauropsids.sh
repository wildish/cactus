#!/bin/bash
#BSUB -o /data/sauropsids/job.%J.out
#BSUB -e /data/sauropsids/job.%J.err
#BSUB -J sauropsids

source /home/user/cactus/cactus_env/bin/activate


if [ "$LSF_JOB_TIMESTAMP_VALUE" == "" ]; then
  now=`date +%s`
  batchSystem=""
  echo "Running locally"
else
  now=$LSF_JOB_TIMESTAMP_VALUE
  batchSystem=" --batchSystem lsf"
  echo "Running in LSF"
fi

base="/data/sauropsids"
work="$base/$now"
mkdir -p $work
cd $work

cmd="cactus $work/jobStore $base/sauropsids.txt $work/output \
  --stats \
  $batchSystem \
  --binariesMode singularity \
  --defaultDisk 20G \
  --maxNodes 45 \
  --maxCores 720 \
  --disableCaching \
  --maxLocalJobs 32 \
  --logFile $work/logfile.log"

echo "Command: $cmd"
$cmd
