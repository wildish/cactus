resource "null_resource" "install_cactus_bastion" {

  connection {
    user        = "${local.user}"
    private_key = "${file(local.private_key)}"
    agent       = false
    host        = "${local.floating_ip}"
  }

  provisioner "file" {
    source      = "../cactus"
    destination = "/home/${local.user}"
  }

  provisioner "remote-exec" {
    inline = [
      "chmod +x /home/${local.user}/cactus/*",
      "/home/${local.user}/cactus/install-cactus-prerequisites.sh",
      "/home/${local.user}/cactus/install-kyoto-cabinet.sh",
      "/home/${local.user}/cactus/install-cactus.sh",
    ]
  }
}

resource "null_resource" "install_cactus_hosts" {

  depends_on = [
    "null_resource.install_cactus_bastion",
  ]

  count = "${local.host_count}"

  connection {
    user                = "${local.user}"
    private_key         = "${file(local.private_key)}"
    agent               = false
    host                = "${element(local.host_ips, count.index)}"
    bastion_private_key = "${file(local.private_key)}"
    bastion_host        = "${local.floating_ip}"
    bastion_user        = "${local.user}"
  }

  provisioner "file" {
    source      = "../cactus"
    destination = "/home/${local.user}"
  }

  provisioner "remote-exec" {
    inline = [
      "chmod +x /home/${local.user}/cactus/*",
      "/home/${local.user}/cactus/install-cactus-prerequisites.sh",
      "/home/${local.user}/cactus/install-kyoto-cabinet.sh",
      "/home/${local.user}/cactus/install-cactus.sh",
    ]
  }
}
