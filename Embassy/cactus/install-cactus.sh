#!/bin/bash

dir=/home/user/cactus
if [ ! -d $dir ]; then
  mkdir -p $dir
fi
cd $dir

which virtualenv 2>/dev/null
if [ $? -ne 0 ]; then
  echo "Need virtualenv to be installed"
  exit 0
fi

[ -d cactus ] || git clone https://github.com/ComparativeGenomicsToolkit/cactus.git

if [ ! -d cactus_env ]; then
  virtualenv cactus_env
  source cactus_env/bin/activate
  pip install --upgrade git+https://github.com/BD2KGenomics/toil
  deactivate
fi

source cactus_env/bin/activate

which cactus 2>/dev/null
if [ $? -eq 0 ]; then
  echo "cactus is already installed"
  exit 0
fi

cd cactus
pip install --upgrade .

git submodule update --init # --remote
export NPROCS=`cat /proc/cpuinfo | grep -c processor`
make # -j $NPROCS
# Add bin to your path
