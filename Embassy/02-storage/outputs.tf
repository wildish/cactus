output "bastion_volumes" {
  value = "${openstack_blockstorage_volume_v2.bastion_volumes.*.metadata}"
}

output "bastion_volume_ids" {
  value = "${openstack_blockstorage_volume_v2.bastion_volumes.*.id}"
}