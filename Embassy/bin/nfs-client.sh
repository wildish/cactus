#!/bin/bash

dir=$1

if [ "$dir" == "" ]; then
  echo "No mountpoint given"
  exit 1
fi

if [ -d $dir/lost+found ]; then
  echo "$dir is already mounted"
  exit 0
fi

server=$2
if [ "$server" == "" ]; then
  echo "Need IP address of server"
  exit 1
fi

options="$3"
if [ "$options" == "" ]; then
  echo "No mount options given, mounting r/o"
  options="defaults,ro"
fi
mkdir -p $dir

echo "${server}:$dir $dir   nfs ${options} 0 0" | tee -a /etc/fstab
mount $dir
