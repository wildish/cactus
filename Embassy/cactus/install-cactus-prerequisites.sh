#!/bin/bash

sudo yum install -y bzip2
sudo ln -s /usr/lib64/libbz2.so{.1,}

dir=/home/user/cactus
if [ ! -d $dir ]; then
  sudo mkdir -p $dir
  sudo chown $(whoami) $dir
  sudo chgrp cactus $dir
fi
cd $dir

which virtualenv 2>/dev/null
if [ $? -ne 0 ]; then
  sudo pip install virtualenv
fi
