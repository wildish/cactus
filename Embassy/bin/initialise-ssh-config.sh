#!/bin/bash
#
# This script prepares the initial ssh config file for ssh'ing into the bastion.
#
state=$1
user=$2
ip=$3
key=$4

if [ "$key" == "" ]; then
  echo "Need a directory, a user, an IP, and an ssh key"
  exit 1
fi

echo "Got state=$state, user=$user, ip=$ip, key=$key"

key=$(cd $state; pwd)/$key
realpath=`which realpath`
if [ "$realpath" != "" ]; then
  key=`realpath $key`
else
  key=`echo $key | sed -e 's%/[^/]*/\.\./%/%g' -e 's%\/\/%\/%g'`
fi

(
  echo "Host *"
  echo "  IdentityFile \"$key\""
  echo "  StrictHostKeyChecking no"
  echo "  UserknownHostsFile /dev/null"
  echo "  User $user"
  echo ""
  echo "Host bastion"
  echo "  HostName $ip"
) | tee $state/ssh-config.bastion \
  | tee $state/ssh-config
chmod 600 $state/ssh-config >/dev/null