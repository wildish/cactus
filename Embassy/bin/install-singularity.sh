#!/bin/bash

sudo yum install -y libarchive-devel squashfs-tools

set -ex
vsn=$1
if [ "$vsn" == "" ]; then
 vsn=2.5.2
fi

file=singularity-${vsn}.tar.gz
url=https://github.com/singularityware/singularity/releases/download/${vsn}/$file
dir=singularity-${vsn}
[ -d $dir ] && rm -rf $dir
[ -f $file ] || wget -O $file $url

tar xf $file
cd $dir
./configure --prefix=/usr/local

ncpus=`cat /proc/cpuinfo  | grep -c processor`
export NPROCS=${NPROCS:$ncpus}
make -j ${NPROCS}

sudo make install
cd ..
rm -rf $dir
