resource "openstack_compute_secgroup_v2" "LSF" {
  name        = "${local.namespace}-LSF"
  description = "Allow LSF traffic"
  rule {
    ip_protocol = "tcp"
    from_port   = 7869
    to_port     = 7869
    cidr        = "0.0.0.0/0"
  }

  rule {
    ip_protocol = "tcp"
    from_port   = 6878
    to_port     = 6878
    cidr        = "0.0.0.0/0"
  }

  rule {
    ip_protocol = "tcp"
    from_port   = 6881
    to_port     = 6881
    cidr        = "0.0.0.0/0"
  }

  rule {
    ip_protocol = "tcp"
    from_port   = 6882
    to_port     = 6882
    cidr        = "0.0.0.0/0"
  }

  rule {
    ip_protocol = "tcp"
    from_port   = 6891
    to_port     = 6891
    cidr        = "0.0.0.0/0"
  }
}
