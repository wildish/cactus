resource "openstack_compute_secgroup_v2" "firewall" {
  name        = "${local.namespace}-firewall"
  description = "Allow SSH and ICMP traffic"

  rule {
    ip_protocol = "icmp"
    from_port   = -1
    to_port     = -1
    cidr        = "0.0.0.0/0"
  }

  rule {
    ip_protocol = "tcp"
    from_port   = 22
    to_port     = 22
    cidr        = "0.0.0.0/0"
  }

  rule {
    ip_protocol = "udp"
    from_port   = 1
    to_port     = 65535
    cidr        = "10.0.0.0/24"
  }
}
