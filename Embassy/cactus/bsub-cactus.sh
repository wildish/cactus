#!/bin/bash
#BSUB -o /home/user/cactus/test.%J.out
#BSUB -e /home/user/cactus/test.%J.err

dir=/home/user/cactus/
source $dir/cactus_env/bin/activate

cd $dir
export PATH=${PATH}:$dir/cactus/bin

time cactus \
  example_lsf_singularity-`date +%s` \
  ./cactus/examples/evolverMammals.txt  \
  ./example_lsf_singularity/result.hal \
  --stats \
  --batchSystem lsf \
  --binariesMode singularity \
  --configFile /home/user/cactus/configs/REL-1405-all.config.xml
