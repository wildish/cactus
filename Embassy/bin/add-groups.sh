#!/bin/bash

file=$1
if [ "$file" == "" ]; then
  echo "Need a json file with user information"
  exit 1
fi

length=`cat $file | jq length`
if [ "$length" == "" ]; then
  echo "No group information, exiting..."
  exit 0
fi

index=0
while [ $index -lt $length ]
do
  group=`cat $file | jq .[${index}].user | tr -d '"'`
  id=`   cat $file | jq .[${index}].id   | tr -d '"'`

  if [ "$id" == "null" ]; then
    opt_id="--gid $id"
  else
    opt_id=""
  fi

  echo "Add $group($id) to bastion and hosts"
  index=`expr 1 + $index`

  echo "Install $group($id) on host `hostname -s`"

  groupadd --force $group $opt_id

done

echo "All done..."
