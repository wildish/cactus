#!/bin/bash
(
  set -ex
  vsn=$1
  if [ "$vsn" == "" ]; then
    echo "Need the LSF version number"
    exit 1
  fi

  dest=/lsf/${vsn}
  id -u lsfadmin || adduser --uid 1001 --user-group lsfadmin

  if [ "`which firewall-cmd`" !=  "" ]; then
    # I'm on CentOS, or an RH derivative
    firewall-cmd --permanent --zone=public --add-port=7869/tcp
    firewall-cmd --permanent --zone=public --add-port=7869/udp
    firewall-cmd --permanent --zone=public --add-port=6878/tcp
    firewall-cmd --permanent --zone=public --add-port=6881/tcp
    firewall-cmd --permanent --zone=public --add-port=6882/tcp
    firewall-cmd --permanent --zone=public --add-port=6891/tcp
    firewall-cmd --reload
  fi

  # I am not a worker, therefore I'm the master - do the master install!
  if [ `hostname` == "bastion" ]; then
    # I have the LSF installation package, therefore I'm the master!
    echo "I am the master"
    cd LSF

    cluster=$2
    if [ "$cluster" == "" ]; then
      echo "Need a cluster name"
      exit 1
    fi

    ip_addr=$3
    if [ "$ip_addr" == "" ]; then
      echo "Need my IP address too"
      exit 1
    fi

    network=$4
    if [ "$network" == "" ]; then
      netmask=`ipcalc -m ${ip_addr} | cut -d= -f 2`
      network=`ipcalc -n ${ip_addr} ${netmask} | cut -d= -f 2 | sed -e 's%\.0$%%' -e 's%\.0$%%'`
    fi

    dir=lsf${vsn}_lsfinstall
    file=lsf${vsn}_lsfinstall_linux_x86_64.tar
    fileZ=${file}.Z

    [ -f $file ] || gunzip $fileZ
    [ -d $dir ] && rm -rf $dir
    [ -d $dest ] && rm -rf $dest
    tar xf $file

    yum install -y ed.x86_64 mailx net-tools java-1.8.0-openjdk
    licence=lsf.entitlement

    mkdir -p $dest
    cp $licence /lsf
    chmod 640 $license /lsf/$licence

    cd $dir
    config="install.config"
    mv $config{,.orig}

    cat ${config}.orig \
      | sed \
        -e "s%^.*LSF_TOP=.*$%LSF_TOP=$dest%" \
        -e "s%^.*LSF_ADMINS=.*$%LSF_ADMINS=lsfadmin%" \
        -e "s%^.*LSF_CLUSTER_NAME=.*$%LSF_CLUSTER_NAME=$cluster%" \
        -e "s%^.*LSF_MASTER_LIST=.*$%LSF_MASTER_LIST=bastion%" \
        -e "s%^.*LSF_ENTITLEMENT_FILE=.*$%LSF_ENTITLEMENT_FILE=/lsf/$licence%" \
        -e "s%^.*SILENT_INSTALL=.*$%SILENT_INSTALL=Y%" \
        -e "s%^.*LSF_SILENT_INSTALL_TARLIST=.*$%LSF_SILENT_INSTALL_TARLIST=all%" \
      | egrep -v 'LSF_PROCESS_TRACKING|LSF_LINUX_CGROUP_ACCT|LSB_RESOURCE_ENFORCE' \
      | tee $config

    ./lsfinstall -f $config

    chmod 640 ${dest}/conf/${license}
    chmod 755 ${dest}/conf/
    echo "LSF_DYNAMIC_HOST_WAIT_TIME=30" | tee -a ${dest}/conf/lsf.conf

  # Now the cluster-specific parts
    cluster_conf=${dest}/conf/lsf.cluster.${cluster}
    mv $cluster_conf{,.orig}
    cat ${cluster_conf}.orig \
      | egrep -v '^#[a-z]' \
      | sed \
        -e "s%^.*LSF_HOST_ADDR_RANGE=.*$%LSF_HOST_ADDR_RANGE=${network}%" \
      | tee $cluster_conf

    # cp $dest/{conf/,}cshrc.lsf
    # cp $dest/{conf/,}profile.lsf
  fi

  #
  # This part is common to all nodes, bastion plus workers
  set +ex
  # [ -L /etc/profile.d/lsf.csh ] || ln -s $dest/cshrc.lsf /etc/profile.d/lsf.csh
  # [ -L /etc/profile.d/lsf.sh  ] || ln -s $dest/profile.lsf /etc/profile.d/lsf.sh
  [ -L /etc/profile.d/lsf.csh ] || ln -s $dest/conf/cshrc.lsf /etc/profile.d/lsf.csh
  [ -L /etc/profile.d/lsf.sh  ] || ln -s $dest/conf/profile.lsf /etc/profile.d/lsf.sh

  while [ `cat /etc/profile.d/lsf.sh | wc -l` -eq 0 ]
  do
    echo "/etc/profile.d/lsf.sh not ready, why does this happen...?"
    sleep $(( RANDOM % 8 + 9 )) # Why isn't the file ready when I can link it...?
  done
  echo "Starting LSF daemons"
  source /etc/profile.d/lsf.sh
  lsf_daemons start
  lsf_daemons status

  #
  # To prevent the bastion from running jobs...
  # cd $dest/conf/lsbatch/${vsn}/configdir
  # lsb_hosts=lsb.hosts
  # mv ${lsb_hosts}{,.orig}
  # cat ${lsb_hosts}.orig \
  #   | awk "{ print }/^HOST_NAME/ { print \"bastion 0 () () () () () ()\"}" \
  #   | tee ${lsb_hosts} >/dev/null
  
) 2>&1 | tee /tmp/install-lsf.log
