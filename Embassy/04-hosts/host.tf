data "openstack_images_image_v2" "host_image" {
  name        = "${local.host_image}"
  most_recent = true
}

locals {
  host_image_id = "${data.openstack_images_image_v2.host_image.id}"
}

resource "openstack_compute_instance_v2" "host" {

  # depends_on = [
  #   # Need explicit dependency for OTC because the network can't be found until the router is up.
  #   "openstack_networking_router_v2.router",
  # ]

  availability_zone = "${local.availability_zone}"
  flavor_name       = "${local.host_flavour}"
  security_groups   = [ "${local.security_groups}" ]
  name              = "${format("%s-h-%04d", local.namespace, count.index)}"

  image_id = "${local.host_image_id}"
  # block_device {
  #   uuid                  = "${local.host_image_id}"
  #   boot_index            = 0
  #   delete_on_termination = true
  #   destination_type      = "volume"
  #   source_type           = "image"
  #   volume_size           = "${local.host_disk_gb}"
  # }

  network {
    name = "${local.network}"
  }

  key_pair = "${local.key_pair}"

  count = "${local.host_count}"
}

resource "null_resource" "host_post_system_config" {

  depends_on = [
    # "null_resource.bastion_post_config",
    "openstack_compute_instance_v2.host",
  ]

  # triggers { something = "${uuid()}" }

  count = "${local.host_count}"
  connection {
    user                = "${local.user}"
    private_key         = "${file(local.private_key)}"
    agent               = false
    host                = "${element(openstack_compute_instance_v2.host.*.access_ip_v4, count.index)}"
    bastion_private_key = "${file(local.private_key)}"
    bastion_host        = "${local.floating_ip}"
    bastion_user        = "${local.user}"
  }

  provisioner "file" {
    source      = "../bin"
    destination = "/home/${local.user}"
  }
  provisioner "remote-exec" {
    inline = [
      "chmod 755 /home/${local.user}/bin/*",
    ]
  }

  provisioner "file" {
    content = "${jsonencode(local.storage)}"
    destination = "/home/${local.user}/volume.metadata.json"
  }

  provisioner "remote-exec" {
    inline = [
      "sudo /home/${local.user}/bin/yum-packages.sh",
      "sudo /home/${local.user}/bin/fix-hostname.sh ${local.namespace}",
      "sudo /home/${local.user}/bin/set-bastion-host-entry.sh ${local.bastion_ip} ${local.namespace}",
      "sudo /home/${local.user}/bin/firewalld.sh",
      "sudo /home/${local.user}/bin/mount-nfs-disks.sh /home/${local.user}/volume.metadata.json",
      "sudo setsebool -P use_nfs_home_dirs 1", # Tell SELINUX to allow SSH to work with NFS home directories
    ]
  }
}

resource "null_resource" "host_post_user_config" {

  depends_on = [
    "null_resource.host_post_system_config",
  ]

  # triggers { something = "${uuid()}" }

  count = "${local.host_count}"
  connection {
    user                = "${local.user}"
    private_key         = "${file(local.private_key)}"
    agent               = false
    host                = "${element(openstack_compute_instance_v2.host.*.access_ip_v4, count.index)}"
    bastion_private_key = "${file(local.private_key)}"
    bastion_host        = "${local.floating_ip}"
    bastion_user        = "${local.user}"
  }

  provisioner "file" {
    source      = "${local.private_key}"
    destination = "/home/${local.user}/.ssh/ssh-key"
  }
  provisioner "remote-exec" {
    inline = [
      "sudo /home/${local.user}/bin/sshd-fix.sh",
      "/home/${local.user}/bin/ssh-host-config.sh",
    ]
  }

  provisioner "local-exec" {
    command = "../bin/ip-fragment.sh ${local.state} ${element(openstack_compute_instance_v2.host.*.access_ip_v4, count.index)} ${element(openstack_compute_instance_v2.host.*.name, count.index)}"
  }
}

resource "null_resource" "local_ssh_config" {
  
  depends_on = [ "null_resource.host_post_user_config" ]

  triggers { something = "${uuid()}" }

  provisioner "local-exec" {
    command = "../bin/complete-ssh-config.sh ${local.state} ${local.namespace}"
  }
}
