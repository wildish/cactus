#!/bin/sh

namespace=$1
if [ "$namespace" == "" ]; then
  echo "Need a namespace"
  exit 1
fi

#cat /etc/hostname | awk -F\. '{ print $1 }' | sed -e 's%^[^-]*-%%' | tee /etc/hostname.short
cat /etc/hostname | awk -F\. '{ print $1 }' | sed -e "s%^${namespace}-%%" | tee /etc/hostname.short
/bin/mv /etc/hostname{.short,}
hostname -b `cat /etc/hostname`
