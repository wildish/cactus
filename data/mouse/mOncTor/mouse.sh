#!/bin/bash
#BSUB -o /data/mouse/mOncTor/job.%J.out
#BSUB -e /data/mouse/mOncTor/job.%J.err
#BSUB -J mus-mOncTor

source /home/user/cactus/cactus_env/bin/activate

if [ "$LSF_JOB_TIMESTAMP_VALUE" == "" ]; then
  now=`date +%s`
  batchSystem=""
  echo "Running locally"
else
  now=$LSF_JOB_TIMESTAMP_VALUE
  batchSystem=" --batchSystem lsf"
  echo "Running in LSF"
fi

base="/data/mouse/mOncTor"
work="$base/work"
mkdir -p $work
cd $work

cmd="cactus $work/jobStore $base/mouse.txt $work/output \
  --stats \
  $batchSystem \
  --binariesMode singularity \
  --defaultDisk 20G \
  --maxNodes 45 \
  --maxCores 720 \
  --disableCaching \
  --maxLocalJobs 32 \
  --logFile $work/logfile.log"
echo "Command: $cmd"
$cmd
