#!/bin/bash

state=$1
file=$2
if [ "$file" == "" ]; then
  echo "Need the state directory and a json file with user information"
  exit 1
fi

length=`cat $file | jq length`
if [ "$length" == "" ]; then
  echo "No user information, exiting..."
  exit 0
fi

key_dir=${state}/keys
#[ -d $key_dir ] && rm -rf $key_dir
#mkdir -p $key_dir

index=0
while [ $index -lt $length ]
do
  user=`cat $file | jq .[${index}].user | tr -d '"'`
  ssh=` cat $file | jq .[${index}].ssh  | tr -d '"'`
  key=` cat $file | jq .[${index}].key  | tr -d '"'`

  index=`expr 1 + $index`

  key_file=${key_dir}/key.$user
  if [ -f $key_file ]; then
    echo "$key_file exists, won't clobber"
  else
    ssh-keygen -t rsa -b 4096 -C "Auto-generated key" -P "" -f $key_file
  fi

  if [ "$key" != "null" ]; then
    key_file="${key_file}.pub.provided"
    [ -f $key_file ] && rm -f $key_file
    echo $key | tee $key_file >/dev/null
  fi
done

echo "All done..."
